import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsecasesComponent} from './pages/about/usecases/usecases.component';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './pages/layout/layout.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {TeamlistComponent} from "./pages/team";
import {UserComponent} from "./pages/user/user.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '',   redirectTo: 'dashboard', pathMatch: 'full' },
      {path: 'about', pathMatch: 'full', component: UsecasesComponent},
      {path: 'dashboard', pathMatch: 'full', component: DashboardComponent},
      {path: 'user/:id', pathMatch: 'full', component: UserComponent},
      {
        path: 'teams',
        loadChildren: () =>
          import('./pages/team/team.module').then((m) => m.TeamModule),
      },
      {
        path: 'matches',
        loadChildren: () =>
          import('./pages/match/match.module').then((m) => m.MatchModule),
      },
      {
        path: 'auth',
        loadChildren: () =>
          import('./pages/auth/auth.module').then((m) => m.AuthModule)
      }

    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule {
}
