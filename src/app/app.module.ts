import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {UsecasesComponent} from './pages/about/usecases/usecases.component';
import {AppRoutingModule} from './app-routing.module';
import {ToolbarComponent} from './pages/shared/toolbar/toolbar.component';
import {LayoutComponent} from './pages/layout/layout.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {LayoutModule} from '@angular/cdk/layout';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {LeaguetableComponent} from './pages/dashboard/leaguetable/leaguetable.component';
import {MatTableModule} from '@angular/material/table';
import {UsecaseComponent} from './pages/about/usecases/usecase/usecase.component';
import { UpcomingComponent } from './pages/dashboard/upcoming/upcoming.component';
import {MatCarouselModule} from "@ngbmodule/material-carousel";
import { RecentComponent } from './pages/dashboard/recent/recent.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './helpers/token.interceptor';
import {MatPaginatorModule} from "@angular/material/paginator";
import { UserComponent } from './pages/user/user.component';
import { FollowingComponent } from './pages/user/following/following.component';
import { FollowersComponent } from './pages/user/followers/followers.component';
import { DashboardusersComponent } from './pages/dashboard/dashboardusers/dashboardusers.component';

@NgModule({
  declarations: [
    AppComponent,
    UsecasesComponent,
    ToolbarComponent,
    LayoutComponent,
    DashboardComponent,
    LeaguetableComponent,
    UsecaseComponent,
    UpcomingComponent,
    RecentComponent,
    UserComponent,
    FollowingComponent,
    FollowersComponent,
    DashboardusersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    HttpClientModule,
    MatCarouselModule,
    ReactiveFormsModule,
    MatInputModule,
    MatPaginatorModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
