import {Component, OnInit, Input} from '@angular/core';
import {UseCase} from '../usecase.model';

@Component({
  selector: 'app-usecase',
  template: `
    <table style="font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%; text-align: start;">
      <tbody>
      <tr style="background-color: #dddddd;">
        <th scope="row" style="width: 16.66%; border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Naam
        </th>
        <td style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">
          <strong>{{ useCase.id }} {{ useCase.name }}</strong>
        </td>
      </tr>
      <tr>
        <th scope="row"  style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Beschrijving</th>
        <td>{{ useCase.description }}</td>
      </tr>
      <tr style="background-color: #dddddd;">
        <th scope="row"  style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Actor</th>
        <td>{{ useCase.actor }}</td>
      </tr>
      <tr>
        <th scope="row"  style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Preconditie</th>
        <td>{{ useCase.precondition }}</td>
      </tr>
      <tr style="background-color: #dddddd;">
        <th scope="row"  style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Scenario</th>
        <td style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">
          <ol>
            <li *ngFor="let step of useCase.scenario">{{ step }}</li>
          </ol>
        </td>
      </tr>
      <tr>
        <th scope="row"  style="border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Postconditie</th>
        <td>{{ useCase.postcondition }}</td>
      </tr>
      </tbody>
    </table>
    <hr>
  `,
})
export class UsecaseComponent implements OnInit {
  @Input() useCase: UseCase;

  constructor() {
  }

  ngOnInit(): void {
    console.log('UsecaseComponent onInit');
  }
}
