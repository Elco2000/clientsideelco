import {Component, OnInit} from '@angular/core';
import {UseCase} from './usecase.model';

@Component({
  selector: 'app-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css']
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Team maken',
      description: 'Hiermee kan een administrator een team maken die aan de competitie kan meedoen',
      scenario: [
        'Administrator gaat naar lijst met teams',
        'Administrator drukt op de knop create',
        'Administrator voert de informatie in en bevestigd dit',
        'De applicatie valideert de ingevoerde informatie',
        'Indien de gegevens voldoen aan de applicatie zijn eisen wordt de administrator weer terug gestuurd naar de lijst met daarin het ' +
        'nieuwe team'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het team is aangemaakt en kan gevonden worden in de applicatie.',
    },
    {
      id: 'UC-03',
      name: 'Team aanpassen',
      description: 'Hiermee kan een administrator een team aanpassen',
      scenario: [
        'Administrator gaat naar lijst met teams',
        'Administrator drukt op de knop edit bij het team',
        'Administrator voert de informatie in en bevestigd dit',
        'De applicatie valideert de ingevoerde informatie',
        'Indien de gegevens voldoen aan de applicatie zijn eisen wordt de administrator weer terug gestuurd naar de lijst met daarin de teams'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het team is aangepast en kan gevonden worden in de applicatie.',
    },
    {
      id: 'UC-04',
      name: 'Team deleten',
      description: 'Hiermee kan een administrator een team verwijderen',
      scenario: [
        'Administrator gaat naar lijst met teams',
        'Administrator drukt op de knop delete bij het team',
        'Administrator bevestigd dit door een melding dat verschijnt op het scherm',
        'De applicatie verwijderd het team',
        'Administrator wordt weer terug gestuurd naar de lijst zonder de verwijderde team'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het team is verwijderd en is niet meer terug te vinden in de applicatie.',
    },
    {
      id: 'UC-05',
      name: 'Team details bekijken',
      description: 'Hiermee kan een administrator een team specifiek bekijken',
      scenario: [
        'Administrator gaat naar lijst met teams',
        'Administrator drukt op de naam van een team',
        'De applicatie stuurt de administrator naar de juiste pagina',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De administrator krijgt de detail pagina van een team te zien.',
    },
    {
      id: 'UC-06',
      name: 'Player maken',
      description: 'Hiermee kan een administrator een Player maken.',
      scenario: [
        'Administrator gaat naar een team',
        'Administrator drukt op de knop create bij het kopje players',
        'Administrator voert de informatie in en bevestigd dit',
        'De applicatie valideert de ingevoerde informatie',
        'Indien de gegevens voldoen aan de applicatie zijn eisen wordt de administrator weer terug gestuurd naar het team met daarin het ' +
        'de nieuwe player'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De player is aangemaakt en kan gevonden worden in de applicatie.',
    },
    {
      id: 'UC-07',
      name: 'Player aanpassen',
      description: 'Hiermee kan een administrator een player aanpassen',
      scenario: [
        'Administrator gaat naar een team',
        'Administrator drukt op de knop edit bij de juiste player',
        'Administrator voert de informatie in en bevestigd dit',
        'De applicatie valideert de ingevoerde informatie',
        'Indien de gegevens voldoen aan de applicatie zijn eisen wordt de administrator weer terug gestuurd naar het team met daarin de players'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De player is aangepast en kan gevonden worden in de applicatie.',
    },
    {
      id: 'UC-08',
      name: 'Player deleten',
      description: 'Hiermee kan een administrator een player verwijderen',
      scenario: [
        'Administrator gaat naar een team',
        'Administrator drukt op de knop delete bij de juiste player',
        'Administrator bevestigd dit door een melding dat verschijnt op het scherm',
        'De applicatie verwijderd de player',
        'Administrator wordt weer terug gestuurd naar het team zonder de verwijderde player'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De player is verwijderd en is niet meer terug te vinden in de applicatie.',
    },
    {
      id: 'UC-09',
      name: 'Player details bekijken',
      description: 'Hiermee kan een administrator een player specifiek bekijken',
      scenario: [
        'Administrator gaat naar een team',
        'Administrator drukt op de naam van de juiste player',
        'De applicatie stuurt de administrator naar de juiste pagina',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De administrator krijgt de detail pagina van een player te zien.',
    },
    {
      id: 'UC-10',
      name: 'Match maken',
      description: 'Hiermee kan een administrator een match maken',
      scenario: [
        'Administrator gaat naar lijst met matches',
        'Administrator drukt op de knop create',
        'Administrator voert de informatie in en bevestigd dit',
        'De applicatie valideert de ingevoerde informatie',
        'Indien de gegevens voldoen aan de applicatie zijn eisen wordt de administrator weer terug gestuurd naar de lijst met daarin het ' +
        'nieuwe match'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De match is aangemaakt en kan gevonden worden in de applicatie.',
    },
    {
      id: 'UC-11',
      name: 'Match aanpassen',
      description: 'Hiermee kan een administrator een match aanpassen',
      scenario: [
        'Administrator gaat naar lijst met matches',
        'Administrator drukt op de knop edit bij de match',
        'Administrator voert de informatie in en bevestigd dit',
        'De applicatie valideert de ingevoerde informatie',
        'Indien de gegevens voldoen aan de applicatie zijn eisen wordt de administrator weer terug gestuurd naar de lijst met daarin de matches'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De match is aangepast en kan gevonden worden in de applicatie.',
    },
    {
      id: 'UC-12',
      name: 'Match deleten',
      description: 'Hiermee kan een administrator een match verwijderen',
      scenario: [
        'Administrator gaat naar lijst met matches',
        'Administrator drukt op de knop delete bij de match',
        'Administrator bevestigd dit door een melding dat verschijnt op het scherm',
        'De applicatie verwijderd de match',
        'Administrator wordt weer terug gestuurd naar de lijst zonder de verwijderde match'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De match is verwijderd en is niet meer terug te vinden in de applicatie.',
    },
    {
      id: 'UC-13',
      name: 'Ranklijst bekijken',
      description: 'Hiermee kan een gebruiker de huidige stand zien van de competitie',
      scenario: [
        'De gebruiker gaat naar het dashboard',
        'De applicatie genereert de gegevens'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Nvt',
      postcondition: 'De gebruiker ziet de ranklijst op de dashboard pagina',
    },
    {
      id: 'UC-14',
      name: 'Aankomende wedstrijden bekijken',
      description: 'Hiermee kan een gebruiker een aantal aankomende wedstrijden zien',
      scenario: [
        'De gebruiker gaat naar het dashboard',
        'De applicatie genereert de gegevens'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Nvt',
      postcondition: 'De gebruiker ziet een aantal aankomende wedstrijden op de dashboard pagina',
    },
    {
      id: 'UC-15',
      name: 'Recente wedstrijden bekijken',
      description: 'Hiermee kan een gebruiker een aantal recente wedstrijden zien',
      scenario: [
        'De gebruiker gaat naar het dashboard',
        'De applicatie genereert de gegevens'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Nvt',
      postcondition: 'De gebruiker ziet een aantal recente wedstrijden op de dashboard pagina',
    },
    {
      id: 'UC-16',
      name: 'Gebruiker kan een team volgen',
      description: 'Hiermee kan een gebruiker een team volgen en krijgt hierdoor wedstrijden van dat team te zien op zijn profiel',
      scenario: [
        'De gebruiker gaat naar het dashboard',
        'Selecteert een team via de naam in de ranklijst',
        'De applicatie stuurt de gebruiker naar de team detail pagina',
        'De gebruiker klikt op het follow knopje',
        'De applicatie verwerkt de follow request'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker moet ingelogd zijn',
      postcondition: 'De gebruiker ziet wedstrijden op zijn profiel van teams die hij volgt',
    },
    {
      id: 'UC-17',
      name: 'Gebruiker kan met een andere gebruiker vrienden worden',
      description: 'Hiermee kan een gebruiker een vriend toevoegen en makkelijk zijn profiel bekijken',
      scenario: [
        'De gebruiker zoekt naar een andere gebruiker',
        'Selecteert de gewenste gebruiker',
        'De applicatie stuurt de gebruiker naar de gebruiker pagina',
        'De gebruiker klikt op het be friends knopje',
        'De applicatie verwerkt de friends request'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Gebruiker moet ingelogd zijn',
      postcondition: 'De gebruiker ziet zijn vriend tussen zijn vriendenlijst staan',
    },
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
