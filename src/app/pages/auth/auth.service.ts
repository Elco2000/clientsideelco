import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {User} from "./user.model";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private authUrl = 'https://clientsideapi-elco.herokuapp.com/api/';  // URL to web api
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>

  constructor(private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse((localStorage.getItem('user'))));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }


  login(email, password) {
    return this.http.post<User>(this.authUrl + "login", {email, password})
      .pipe(map(user => {
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }));
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }

  register(user: User) {
    return this.http.post(this.authUrl + "register", user);
  }

  public getToken(): string {
    if (this.userValue !== null) {
      return this.userValue.token;
    } else {
      return "";
    }

  }

}
