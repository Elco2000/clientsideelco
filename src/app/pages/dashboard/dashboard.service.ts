import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

import {Match} from '../match/match.model';
import {Team} from "../team/team.model";
import {UserInformation} from "../user/userInformation.model";
import {User} from "../auth/user.model";


@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private dashboardUrl = 'https://clientsideapi-elco.herokuapp.com/api/matches';  // URL to web api
  private dashboardTeamUrl = 'https://clientsideapi-elco.herokuapp.com/api/teams';  // URL to web api
  private apiUrl = 'https://clientsideapi-elco.herokuapp.com/api';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) { }

  getRecentMatches(): Observable<Match[]> {
    return this.http.get<Match[]>(this.dashboardUrl + '/recent')
  }

  getUpcomingMatches(): Observable<Match[]> {
    return this.http.get<Match[]>(this.dashboardUrl + '/upcoming')
  }

  getMatches(): Observable<Match[]> {
    return this.http.get<Match[]>(this.dashboardUrl)
  };

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.dashboardTeamUrl)
  };

  getUserInformation(id: string): Observable<UserInformation> {
    const url = this.apiUrl + 'users' + '/' + id;
    return this.http.get<UserInformation>(url);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl + '/users');
  }
}
