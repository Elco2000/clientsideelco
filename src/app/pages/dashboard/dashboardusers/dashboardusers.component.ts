import { Component, OnInit } from '@angular/core';
import {DashboardService} from "../dashboard.service";
import {User} from "../../auth/user.model";

@Component({
  selector: 'app-dashboardusers',
  templateUrl: './dashboardusers.component.html',
  styleUrls: ['./dashboardusers.component.css']
})
export class DashboardusersComponent implements OnInit {
  users: User[];

  constructor(private dashboardService : DashboardService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.dashboardService.getUsers()
      .subscribe(users => {
        this.users = users;
      })
  }

}
