import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {DashboardService} from "../dashboard.service";
import {MatTableDataSource} from "@angular/material/table";
import {Team} from "../../team/team.model";
import {MatPaginator} from "@angular/material/paginator";


export interface LeagueTable {
  position: number;
  name: string;
  points: number;
  goals: number;
}

@Component({
  selector: 'app-leaguetable',
  templateUrl: './leaguetable.component.html',
  styleUrls: ['./leaguetable.component.css']
})
export class LeaguetableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'stadium', 'coach'];
  dataSource = new MatTableDataSource();
  teams: Team[];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dashboardService : DashboardService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getTeams(): void {
    this.dashboardService.getTeams()
      .subscribe(teams =>  {
        this.teams = teams;
        this.dataSource.data = this.teams;
      })
  }

}

