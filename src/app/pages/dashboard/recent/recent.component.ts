import { Component, OnInit } from '@angular/core';
import {Match} from "../../match/match.model";
import {DashboardService} from "../dashboard.service";

@Component({
  selector: 'app-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.css']
})
export class RecentComponent implements OnInit {
  matches: Match[];

  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.getRecentMatches();
  }

  getRecentMatches(): void {
    this.dashboardService.getRecentMatches()
      .subscribe(matches => this.matches = matches);
  }

  getDateWithoutTime(date: Date){
    return date.toDateString();
  }

  getScore(result: string, team: number){
    const str = Array.from(result.split("-")[team]);
    return str.join("");
  }

}
