import { Component, OnInit } from '@angular/core';
import {Match} from "../../match/match.model";
import {DashboardService} from "../dashboard.service";

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {
  matches: Match[];

  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.getUpcomingMatches();
  }

  getUpcomingMatches(): void {
    this.dashboardService.getUpcomingMatches()
      .subscribe(matches => this.matches = matches);
  }

  getDateWithoutTime(date: Date){
    return date.toDateString();
  }

}
