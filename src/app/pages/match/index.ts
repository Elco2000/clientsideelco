import {MatchlistComponent} from "./matchlist/matchlist.component";
import {MatchcreateComponent} from "./matchcreate/matchcreate.component";
import {MatcheditComponent} from "./matchedit/matchedit.component";

export const components: any[] = [
  MatchlistComponent,
  MatchcreateComponent,
  MatcheditComponent
]

export * from './matchlist/matchlist.component';
export * from './matchcreate/matchcreate.component';
export * from './matchedit/matchedit.component';
