export class Match {
  _id: string;
  homeTeam: string;
  awayTeam: string;
  result?: string;
  matchDate: Date;
}
