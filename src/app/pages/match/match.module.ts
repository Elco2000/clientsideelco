import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatRadioModule} from "@angular/material/radio";
import {matchNotTheSame} from "./matchedit/matchNotTheSame";
import {AuthGuard} from "../../helpers/auth.guard";

const routes: Routes = [
  {path: '', pathMatch: 'full', component: fromComponents.MatchlistComponent},
  {path: 'create', pathMatch: 'full', component: fromComponents.MatchcreateComponent,
    canActivate: [AuthGuard]},
  {path: 'edit/:id', pathMatch: 'full', component: fromComponents.MatcheditComponent,
    canActivate: [AuthGuard]}
];

@NgModule({
  declarations: [...fromComponents.components, matchNotTheSame],
  imports: [CommonModule, RouterModule.forChild(routes), MatCardModule, MatTableModule, MatMenuModule,
    MatIconModule, MatButtonModule, ReactiveFormsModule, MatFormFieldModule, MatDatepickerModule, MatOptionModule,
    MatSelectModule, MatInputModule, MatNativeDateModule, MatRippleModule, MatRadioModule, FormsModule]
})
export class MatchModule {}
