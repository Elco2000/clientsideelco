import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';

import {Match} from './match.model';

@Injectable({
  providedIn: 'root'
})
export class MatchService {
  private matchUrl = 'https://clientsideapi-elco.herokuapp.com/api/matches';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) { }

  getMatches(): Observable<Match[]> {
    return this.http.get<Match[]>(this.matchUrl)
  };

  /** POST: add a new match to the server */
  addMatch(match: Match): Observable<Match> {
    return this.http.post<Match>(this.matchUrl, match, this.httpOptions);
  }

  getMatch(id: string): Observable<Match> {
    const url = this.matchUrl + '/' + id;
    return this.http.get<Match>(url);
  }

  deleteMatch(id: string): Observable<Match> {
    const url = `${this.matchUrl}/${id}`;
    return this.http.delete<Match>(url, this.httpOptions);
  }

  /** PUT: update the hero on the server */
  updateMatch(match: Match): Observable<any> {
    return this.http.put(this.matchUrl  + '/' + match._id, match, this.httpOptions);
  }
}
