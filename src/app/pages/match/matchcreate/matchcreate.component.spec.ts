import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchcreateComponent } from './matchcreate.component';

describe('MatchcreateComponent', () => {
  let component: MatchcreateComponent;
  let fixture: ComponentFixture<MatchcreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchcreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchcreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
