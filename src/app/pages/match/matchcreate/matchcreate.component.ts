import {Component, OnInit} from '@angular/core';
import {MatchService} from '../match.service';
import {TeamService} from '../../team/team.service';
import {Match} from "../match.model";
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {Location} from '@angular/common';
import {Team} from "../../team/team.model";

@Component({
  selector: 'app-matchcreate',
  templateUrl: './matchcreate.component.html',
  styleUrls: ['./matchcreate.component.css']
})
export class MatchcreateComponent implements OnInit {
  matchForm: FormGroup;
  matches: Match[];
  teams: Team[];

  constructor(private matchService: MatchService, private teamService: TeamService, private location: Location) {
  }

  ngOnInit(): void {
    this.matchForm = new FormGroup({
      homeTeam: new FormControl('', [
        Validators.required,
      ]),
      awayTeam: new FormControl('',[
        Validators.required,
      ]),
      result: new FormControl('', [
        Validators.pattern("(\\d{1,2})-(\\d{1,2})")
      ]),
      matchDate: new FormControl('', [
        Validators.required
      ]),
      // professionalStatus: new FormControl('', [
      //   Validators.required
      // ]),
      // coach: new FormControl('', [
      //   Validators.required,
      //   Validators.minLength(4)
      // ])
    }, { validators: matchValidator});
    this.getMatches();
    this.getTeams();
  }

  getHomeTeam() { return this.matchForm.get('homeTeam'); }
  getAwayTeam() { return this.matchForm.get('awayTeam'); }
  getResult() { return this.matchForm.get('result'); }
  getMatchDate() { return this.matchForm.get('matchDate'); }
  // getProfessionalStatus() { return this.teamForm.get('professionalStatus'); }
  // getCoach() { return this.teamForm.get('coach'); }


  getErrorMessage(element: AbstractControl) {
    if (element.hasError('required')) {
        return ('You must enter a value');
    } else if (element.hasError('pattern')) {
      return ('You must enter a valid score! (..-..)');
    }
  }

  getMatches(): void {
    this.matchService.getMatches()
      .subscribe(matches => this.matches = matches);
  }

  getTeams(): void {
    this.teamService.getTeams()
      .subscribe(teams => this.teams = teams);
  }

  add(homeTeam: string, awayTeam: string, result: string,
      matchDateString: string): void {
    const matchDate = new Date(matchDateString)
    if (!this.matchForm.valid) {
      return;
    } else {
      this.matchService.addMatch({homeTeam, awayTeam, result, matchDate} as Match)
        .subscribe(match => {
          this.matches.push(match);
          this.goBack();
        });
    }
  }

  goBack(): void {
    this.location.back();
  }

}

export const matchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const homeTeam = control.get('homeTeam');
  const awayTeam = control.get('awayTeam');

  return homeTeam && awayTeam && homeTeam.value === awayTeam.value ? { identityRevealed: true } : null;
};
