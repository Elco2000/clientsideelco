import {Directive} from "@angular/core";
import {AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from "@angular/forms";

@Directive({
  selector: '[matchNotTheSame]',
  providers: [{provide: NG_VALIDATORS, useExisting: matchNotTheSame, multi: true}]
})
export class matchNotTheSame implements Validator {
  validate(control: AbstractControl): ValidationErrors {
    return matchValidator(control);
  }
}

// Uitgezet in de html. Deed het niet helemaal zoals de bedoeling was. De formulier wordt namelijk niet uitgevoerd als
// de functie aangeroepen wordt.

export const matchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const homeTeam = control.get('homeTeam');
  const awayTeam = control.get('awayTeam');

  return homeTeam && awayTeam && homeTeam.value === awayTeam.value ? { identityRevealed: true } : null;
};

