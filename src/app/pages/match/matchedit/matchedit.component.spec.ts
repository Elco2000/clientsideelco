import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatcheditComponent } from './matchedit.component';

describe('MatcheditComponent', () => {
  let component: MatcheditComponent;
  let fixture: ComponentFixture<MatcheditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatcheditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatcheditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
