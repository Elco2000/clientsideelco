import {Component, Directive, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {MatchService} from "../match.service";
import {Team} from "../../team/team.model";
import {Match} from "../match.model";
import {TeamService} from "../../team/team.service";
import {AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from "@angular/forms";

@Component({
  selector: 'app-matchedit',
  templateUrl: './matchedit.component.html',
  styleUrls: ['./matchedit.component.css']
})

export class MatcheditComponent implements OnInit {
  teams: Team[];
  match: Match;

  constructor(private matchService: MatchService, private teamService: TeamService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.getMatch();
    this.getTeams();
  }

  getTeams(): void {
    this.teamService.getTeams()
      .subscribe(teams => this.teams = teams);
  }

  getMatch(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.matchService.getMatch(id)
      .subscribe(match => this.match = match);
  }

  save(): void {
    this.matchService.updateMatch(this.match)
      .subscribe(match => {
        this.goBack();
      })
  }

  goBack(): void {
    this.location.back();
  }

}


