import {Component, OnInit} from '@angular/core';

import {Match} from "../match.model";
import {MatchService} from '../match.service'

@Component({
  selector: 'app-matchlist',
  templateUrl: './matchlist.component.html',
  styleUrls: ['./matchlist.component.css']
})
export class MatchlistComponent implements OnInit {
  displayedColumns: string[] = ['homeTeamName', 'awayTeamName', 'stadium', 'result', 'matchDate', 'options']
  matches: Match[];

  constructor(private matchService: MatchService) {
  }

  ngOnInit(): void {
    this.getMatches();
  }

  getMatches(): void {
    this.matchService.getMatches()
      .subscribe(matches => this.matches = matches);
  }

  delete(id: string): void {
    this.matchService.deleteMatch(id).subscribe((data) => {
      this.ngOnInit();
    });
  }

}
