import {Component, OnInit} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {AuthService} from "../../auth/auth.service";
import {User} from "../../auth/user.model";
import {ActivatedRoute, Router} from "@angular/router";
import jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  opened: boolean;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  user: User

  constructor(private breakpointObserver: BreakpointObserver, private authService: AuthService, private router: Router) {
    this.router.events.subscribe(val => {
      this.user = this.authService.userValue
      console.log(this.user);
    })
  }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout();
  }

  getDecodedAccessToken(token: string): any {
    try{
      return jwt_decode(token);
    }
    catch(Error){
      return null;
    }
  }

}
