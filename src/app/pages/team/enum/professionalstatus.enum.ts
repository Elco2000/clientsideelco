export enum professionalStatusEnum {
  'High Professional' = 'High Professional',
  'Medium Professional' = 'Medium Professional',
  'Low Professional' = 'Low Professional'
}
