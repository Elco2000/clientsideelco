import { TeamlistComponent} from "./teamlist/teamlist.component";
import {TeamcreateComponent} from "./teamcreate/teamcreate.component";
import {TeamdetailComponent} from "./teamdetail/teamdetail.component";
import {TeameditComponent} from "./teamedit/teamedit.component";

export const components: any[] = [
  TeamlistComponent,
  TeamcreateComponent,
  TeamdetailComponent,
  TeameditComponent
];

export * from './teamlist/teamlist.component';
export * from './teamcreate/teamcreate.component';
export * from './teamdetail/teamdetail.component';
export * from './teamedit/teamedit.component';
