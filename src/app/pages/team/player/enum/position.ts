export enum position {
  'Goalkeeper' = 'Goalkeeper',
  'Defender Centre' = 'Defender Centre',
  'Defender Right' = 'Defender Right',
  'Defender Left' = 'Defender Left',
  'Defensive midfielder Centre' = 'Defensive midfielder Centre',
  'Defensive midfielder Right' = 'Defensive midfielder Right',
  'Defensive midfielder Left' = 'Defensive midfielder Left',
  'Midfielder Centre' = 'Midfielder Centre',
  'Midfielder Right' = 'Midfielder Right',
  'Midfielder Left' = 'Midfielder Left',
  'Attacking midfielder Centre' = 'Attacking midfielder Centre',
  'Attacking midfielder Right' = 'Attacking midfielder Right',
  'Attacking midfielder Left' = 'Attacking midfielder Left',
  'Forward Centre' = 'Forward Centre',
  'Forward Right' = 'Forward Right',
  'Forward Left' = 'Forward Left'
}
