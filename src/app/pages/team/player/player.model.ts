export class Player {
  firstname: string;
  lastname: string;
  nationality: string;
  dateOfBirth: Date;
  captain: boolean;
  positions: [string];
}
