import {RouterModule, Routes} from "@angular/router";
import * as fromComponents from '.';
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatIconModule} from "@angular/material/icon";
import {MatSelectModule} from "@angular/material/select";
import {MatListModule} from "@angular/material/list";
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from "@angular/material/button";
import {AuthGuard} from "../../../helpers/auth.guard";


const routes: Routes = [
  {path: 'create', pathMatch: 'full', component: fromComponents.PlayercreateComponent,
    canActivate: [AuthGuard]}
];
@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes), MatGridListModule, MatCardModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatIconModule, MatSelectModule, MatListModule, MatCheckboxModule, MatButtonModule]
})
export class PlayerModule {}
