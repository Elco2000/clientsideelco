import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Player} from "./player.model";
import {Team} from "../team.model";

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private playerUrl = 'https://clientsideapi-elco.herokuapp.com/api/teams'

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getPlayers(teamid: string): Observable<Team> {
    return this.http.get<Team>(this.playerUrl + '/' + teamid + '/players');
  }

  addPlayer(player: Player, teamid: string): Observable<any> {
    return this.http.put<Player>(this.playerUrl + '/' + teamid + '/players/create', player, this.httpOptions);
  }

  getPlayer(teamid: string, playerid: string): Observable<Player> {
    return this.http.get<Player>(this.playerUrl + '/' + teamid + '/players/' + playerid);
  }

  deletePlayer(teamid: string, playerid: string): Observable<Player> {
    return this.http.delete<Player>(this.playerUrl + '/' + teamid + '/players/' + playerid, this.httpOptions)
  }

  updatePlayer(player: Player, teamid: string, playerid: string): Observable<any> {
    return this.http.put<Player>(this.playerUrl + '/' + teamid + '/players/' + playerid, player, this.httpOptions);
  }
}
