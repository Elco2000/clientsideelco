import {Component, OnInit} from '@angular/core';
import "@angular/compiler"
import {AbstractControl, Form, FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Team} from "../../team.model";
import {nationality} from "../enum/nationality";
import {TeamService} from "../../team.service";
import {Location} from "@angular/common";
import {PlayerService} from "../player.service";
import {ActivatedRoute} from "@angular/router";
import {Player} from "../player.model";
import {position} from "../enum/position";

@Component({
  selector: 'app-playercreate',
  templateUrl: './playercreate.component.html',
  styleUrls: ['./playercreate.component.css']
})
export class PlayercreateComponent implements OnInit {
  playerForm: FormGroup;
  players: Team;
  positions = position;
  conditionalOperator = nationality;
  enumKeys = [];
  enumKeysPos = [];
  positionsSelected = [];

  constructor(private playerService: PlayerService, private location: Location, private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
    this.enumKeys = Object.keys(this.conditionalOperator);
    this.enumKeysPos = Object.keys(this.positions);
  }

  ngOnInit(): void {
    this.playerForm = new FormGroup({
      firstname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      lastname: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      dateOfBirth: new FormControl('', [
        Validators.required
      ]),
      nationality: new FormControl('', [
        Validators.required
      ])
    })
    this.getPlayers();
  }

  getFirstname() {
    return this.playerForm.get('firstname');
  }

  getLastname() {
    return this.playerForm.get('nationality');
  }

  getDateOfBirth() {
    return this.playerForm.get('dateOfBirth');
  }

  getNationality() {
    return this.playerForm.get('nationality');
  }


  getErrorMessage(element: AbstractControl, length: number) {
    if (element.hasError('required')) {
      return ('You must enter a value');
    } else if (element.hasError('minlength')) {
      return ('The value must have a min length of ' + length);
    }
  }

  getCheckboxValues(ev, data) {
    let obj = data

    if (ev.target.checked) {
      this.positionsSelected.push(obj);
    } else {
      let removeIndex = this.positionsSelected.findIndex(itm => itm.data === data);

      if (removeIndex !== -1)
        this.positionsSelected.splice(removeIndex, 1);
    }
  }

  getPlayers(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.playerService.getPlayers(id)
      .subscribe(players => this.players = players)
  }

  add(firstname: string, lastname: string, nationality: string, dateOfBirthDate: string,
      captainString: string): void {
    const id = this.route.snapshot.paramMap.get('id');
    let captain = false;
    const dateOfBirth = new Date(dateOfBirthDate);
    const positions = this.positionsSelected;

    if (captainString === 'true') {
      captain = true
    }
    if (!this.playerForm.valid) {
      return;
    } else {
      this.playerService.addPlayer({
        firstname, lastname, nationality, dateOfBirth,
        captain, positions
      } as Player, id)
        .subscribe(team => {
          this.goBack();
        })
    }
  }

  goBack(): void {
    this.location.back();
  }
}
