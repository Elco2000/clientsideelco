import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {PlayerService} from "../player.service";
import {ActivatedRoute} from "@angular/router";
import {Player} from "../player.model";
import {Team} from "../../team.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-playerlist',
  templateUrl: './playerlist.component.html',
  styleUrls: ['./playerlist.component.css']
})
export class PlayerlistComponent implements OnInit, AfterViewInit {
  players: Team;
  displayedColumns: string[] = ['Nr', 'firstname', 'lastname', 'nationality', 'dateOfBirth', 'options'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private playerService: PlayerService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getPlayers();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getPlayers(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.playerService.getPlayers(id)
      .subscribe(players => {
        this.players = players;
        this.dataSource.data = players.players;
      });
  }

  delete(playerid: string): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.playerService.deletePlayer(id, playerid).subscribe((data) => {
      this.ngOnInit();
    });
  }
}
