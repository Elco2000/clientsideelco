import {Player} from "./player/player.model";

export class Team {
  _id: string;
  name: string;
  foundedOn: Date;
  stadium: string;
  nickname: string;
  professionalStatus: string;
  coach: string;
  players: [Player];
}
