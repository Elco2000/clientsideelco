import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatListModule} from "@angular/material/list";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import {MatPaginatorModule} from "@angular/material/paginator";
import {PlayerlistComponent} from "./player/playerlist/playerlist.component";
import {AuthGuard} from "../../helpers/auth.guard";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {TokenInterceptor} from "../../helpers/token.interceptor";

const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.TeamlistComponent},
  { path: 'create', pathMatch: 'full', component: fromComponents.TeamcreateComponent,
    canActivate: [AuthGuard]},
  { path: ':id', pathMatch: 'full', component: fromComponents.TeamdetailComponent},
  { path: 'edit/:id', pathMatch: 'full', component: fromComponents.TeameditComponent,
    canActivate: [AuthGuard]},
  {
    path: ':id/players',
    loadChildren: () =>
      import('./player/player.module').then((m) => m.PlayerModule)
  }
];
@NgModule({
  declarations: [...fromComponents.components, PlayerlistComponent],
  imports: [CommonModule, RouterModule.forChild(routes), MatTableModule, MatFormFieldModule, MatCardModule, MatDividerModule, MatButtonModule, MatMenuModule, MatIconModule,
    MatInputModule, MatDatepickerModule, MatNativeDateModule, MatRippleModule, MatGridListModule, MatListModule, FormsModule, ReactiveFormsModule, MatSelectModule, MatPaginatorModule],
})
export class TeamModule {}
