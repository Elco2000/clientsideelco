import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {Team} from './team.model';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private teamUrl = 'https://clientsideapi-elco.herokuapp.com/api/teams';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamUrl);
  };

  /** POST: add a new team to the server */
  addTeam(team: Team): Observable<Team> {
    return this.http.post<Team>(this.teamUrl, team, this.httpOptions);
  }

  getTeam(id: string): Observable<Team> {
    const url = this.teamUrl + '/' + id;
    return this.http.get<Team>(url);
  }

  deleteTeam(id: string): Observable<Team> {
    const url = `${this.teamUrl}/${id}`;
    return this.http.delete<Team>(url, this.httpOptions);
  }

  /** PUT: update the hero on the server */
  updateTeam(team: Team): Observable<any> {
    console.log(localStorage.getItem('token'))
    return this.http.put(this.teamUrl  + '/' + team._id, team, this.httpOptions);
  }
}
