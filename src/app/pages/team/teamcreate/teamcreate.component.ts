import { Component, OnInit } from '@angular/core';
import { TeamService } from '../team.service';
import {Team} from "../team.model";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import { Location } from '@angular/common';
import { professionalStatusEnum } from '../enum/professionalstatus.enum';

@Component({
  selector: 'app-teamcreate',
  templateUrl: './teamcreate.component.html',
  styleUrls: ['./teamcreate.component.css']
})
export class TeamcreateComponent implements OnInit {
  teamForm: FormGroup;
  teams: Team[];
  conditionalOperator = professionalStatusEnum;
  enumKeys=[];

  constructor(private teamService: TeamService, private location: Location) {
    this.enumKeys=Object.keys(this.conditionalOperator);
  }

  ngOnInit(): void {
    this.teamForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      nickname: new FormControl('',[
        Validators.required,
        Validators.minLength(4)
      ]),
      foundedOn: new FormControl('', [
        Validators.required
      ]),
      stadium: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      professionalStatus: new FormControl('', [
        Validators.required
      ]),
      coach: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ])
    })
    this.getTeams();
  }

  getName() { return this.teamForm.get('name'); }
  getNickName() { return this.teamForm.get('nickname'); }
  getFoundedOn() { return this.teamForm.get('foundedOn'); }
  getStadium() { return this.teamForm.get('stadium'); }
  getProfessionalStatus() { return this.teamForm.get('professionalStatus'); }
  getCoach() { return this.teamForm.get('coach'); }


  getErrorMessage(element: AbstractControl, length: number) {
    if(element.hasError('required')) {
      return('You must enter a value');
    } else if (element.hasError('minlength')) {
      return('The value must have a min length of ' + length);
    }
  }

  getTeams(): void {
    this.teamService.getTeams()
      .subscribe(teams => this.teams = teams);
  }

  add(name: string, nickname: string, foundedOnDate: string, stadium: string,
      professionalStatus: string, coach: string): void {
    const foundedOn = new Date(foundedOnDate)
    if(!this.teamForm.valid){
      return;
    } else {
      console.log(professionalStatus);
      this.teamService.addTeam({ name, nickname, foundedOn, stadium, professionalStatus, coach } as Team)
        .subscribe(team => {
          this.teams.push(team);
          this.goBack();
        });
    }
  }

  goBack(): void {
    this.location.back();
  }


}
