import {Component, OnInit} from '@angular/core';
import {TeamService} from "../team.service";
import {Team} from "../team.model";
import {ActivatedRoute} from "@angular/router";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {professionalStatusEnum} from "../enum/professionalstatus.enum";
import {Location} from '@angular/common';

@Component({
  selector: 'app-teamedit',
  templateUrl: './teamedit.component.html',
  styleUrls: ['./teamedit.component.css']
})
export class TeameditComponent implements OnInit {
  team: Team;
  conditionalOperator = professionalStatusEnum;
  enumKeys = [];

  constructor(private teamService: TeamService, private route: ActivatedRoute, private location: Location) {
    this.enumKeys = Object.keys(this.conditionalOperator);
  }

  ngOnInit(): void {
    this.getTeam();
  }


  getTeam(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.teamService.getTeam(id)
      .subscribe(team => this.team = team);
  }

  save(): void {
    this.teamService.updateTeam(this.team)
      .subscribe(team => {
        this.goBack();
      });
  }

  goBack(): void {
    this.location.back();
  }
}
