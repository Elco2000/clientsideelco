import {Component, OnInit} from '@angular/core';

import {Team} from '../team.model';
import {TeamService} from '../team.service';

@Component({
  selector: 'app-teamlist',
  templateUrl: './teamlist.component.html',
  styleUrls: ['./teamlist.component.css']
})
export class TeamlistComponent implements OnInit {
  displayedColumns: string[] = ['name', 'foundedOn', 'stadium', 'nickname', 'professionalStatus', 'coach', 'options'];
  teams: Team[];

  constructor(private teamService: TeamService) {
  }

  ngOnInit(): void {
    this.getTeams();
  }

  getTeams(): void {
    this.teamService.getTeams()
      .subscribe(teams => this.teams = teams);
  }

  delete(id: string): void {
    // const id = this.route.snapshot.paramMap.get('id');
    this.teamService.deleteTeam(id).subscribe((data) => {
      this.ngOnInit();
    });
  }
}
