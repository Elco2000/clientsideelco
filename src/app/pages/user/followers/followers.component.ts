import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../user.service";
import {AuthService} from "../../auth/auth.service";
import {ActivatedRoute} from "@angular/router";
import {UserInformation} from "../userInformation.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {

  usersFollowers: UserInformation[];
  displayedColumns: string[] = ['name', 'email'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService, private authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getFollowers();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getFollowers(): void {
    const id = this.route.snapshot.paramMap.get('id');
    // this.userService.getFollowing(jwtDecode(this.authService.getToken()))
    this.userService.getAnotherFollowing(id)
      .subscribe(usersFollowers => {
        this.usersFollowers = usersFollowers;
        this.dataSource.data = this.usersFollowers;
        console.log(this.usersFollowers)
      })
  }
}
