import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../user.service";
import {AuthService} from "../../auth/auth.service";
import {ActivatedRoute} from "@angular/router";
import {UserInformation} from "../userInformation.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css']
})
export class FollowingComponent implements OnInit {

  usersFollowing: UserInformation[];
  displayedColumns: string[] = ['name', 'email'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService, private authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getFollowing();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getFollowing(): void {
    const id = this.route.snapshot.paramMap.get('id');
    // this.userService.getFollowing(jwtDecode(this.authService.getToken()))
    this.userService.getFollowing(id)
      .subscribe(usersFollowing => {
        this.usersFollowing = usersFollowing;
        this.dataSource.data = this.usersFollowing;
        console.log(this.usersFollowing)
      });
  }

}
