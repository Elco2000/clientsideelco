import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "./user.service";
import {UserInformation} from "./userInformation.model";
import {AuthService} from "../auth/auth.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Location} from "@angular/common";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: UserInformation;
  followStatus: Boolean;
  sameUser: Boolean;
  usersFollowing: UserInformation[];


  constructor(private userService: UserService, private authService: AuthService, private route: ActivatedRoute, private location: Location) {
  }

  ngOnInit(): void {
    this.getUser();
    this.getFollowStatus();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserInformation(id)
      .subscribe(user => this.user = user);
  }

  getFollowStatus(): void {
    const id = this.route.snapshot.paramMap.get('id');
    // const aId = jwt_decode(this.authService.getToken());
    const helper = new JwtHelperService();
    const aId = helper.decodeToken(this.authService.getToken())
    if (id == aId) {
      this.sameUser = true;
      console.log("Same User True");
    } else {
      this.userService.getFollowStatus(id, aId)
        .subscribe(followStatus => {
          this.followStatus = followStatus;
          console.log(aId + " " + id);
          console.log(this.followStatus);
        })
    }
  }

  FollowUser(): void {
    const helper = new JwtHelperService();
    const aId = helper.decodeToken(this.authService.getToken())
    this.userService.FollowUser(aId, this.user)
      .subscribe(user => {
        this.goBack();
      })
  }

  UnfollowUser(): void {
    const helper = new JwtHelperService();
    const aId = helper.decodeToken(this.authService.getToken())
    this.userService.UnfollowUser(aId, this.user)
      .subscribe(user => {
        this.goBack();
      })
  }

  goBack(): void {
    this.location.back();
  }
}
