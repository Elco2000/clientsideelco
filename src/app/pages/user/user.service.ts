import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {UserInformation} from "./userInformation.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../auth/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userInformationUrl = 'https://clientsideapi-elco.herokuapp.com/api/';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) { }

  getUserInformation(id: string): Observable<UserInformation> {
    const url = this.userInformationUrl + 'users' + '/' + id;
    return this.http.get<UserInformation>(url);
  }

  getFollowing(id: string): Observable<UserInformation[]> {
    return this.http.get<UserInformation[]>(this.userInformationUrl + 'relation/if/' + id);
  }

  getAnotherFollowing(id: string): Observable<UserInformation[]> {
    return this.http.get<UserInformation[]>(this.userInformationUrl + 'relation/af/' + id);
  }

  getFollowStatus(id: string, aId: string): Observable<Boolean> {
    return this.http.get<Boolean>(this.userInformationUrl + 'relation/checkstatus/' + aId + "/" + id);
  }

  FollowUser(aId: string, user: UserInformation): Observable<any> {
    return this.http.put(this.userInformationUrl + 'relation/' + aId + "/" + user.id, user, {responseType: 'text'});
  }

  UnfollowUser(aId: string, user: UserInformation): Observable<any> {
    return this.http.put(this.userInformationUrl + 'relation/delete/' + aId + "/" + user.id, user, {responseType: 'text'});
  }
}
