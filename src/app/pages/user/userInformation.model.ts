export class UserInformation {
  id: number;
  name: string;
  email: string;
}
